import json
import sys
from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer


# from apps.chat.models import Chat
from chat.models import Chat,Message


class ChatConsumer(AsyncWebsocketConsumer):

    async def connect(self):
        #self.group = self.scope['url_route']['kwargs']['guid']
        #self.chat_object = await self.get_chat(self.group)
        #print(self.group)
        #print("Messaging started at {0} ".format(self.group))
        #await self.channel_layer.group_add(self.group, self.channel_name)
        await self.accept()
        print(sys._getframe().f_code.co_name)

    async def disconnect(self, close_code):
        # Leave room group
        print("Disconnected", close_code)
        #await self.channel_layer.group_discard(
        #    self.group,
        #    self.channel_name)
        await self.close()

    # Receive message from WebSocket
    async def receive(self, text_data=None, bytes_data=None):
        text_data_json = json.loads(text_data)
        self.message = text_data_json['message']
        try:
            print('Message In')
            print(self.scope['user'])
            sender = self.scope['user'].username
            await self.save_message()
        except Exception as e:
            print(__file__)
            print(e)
        # Send message to room group
        await self.channel_layer.group_send(
            self.group,
            {
                'type': 'chat_message',
                'message': self.message,
                #'sender': sender,

            }
        )
        print(sys._getframe().f_code.co_name)

    # Receive message from room group
    async def chat_message(self, event):
        print(self.scope["user"], sys._getframe().f_code.co_name)
        message = event['message']
        sender = event['sender']
        # Send message to WebSocket
        await self.send(
            text_data=json.dumps(
                {'message': message, 'sender': sender}))

    @database_sync_to_async
    def save_message(self, ):
        Message.objects.create(
            text=self.message,
            sender=self.scope['user'],
            chat=self.chat_object
        )

    @database_sync_to_async
    def get_chat(self, guid):
        chat = Chat.objects.filter(guid=guid)[0]
        return chat
