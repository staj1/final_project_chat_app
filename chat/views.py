from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from django.views.generic.base import View
from rest_framework import generics,status
from rest_framework.serializers import Serializer
from rest_framework.generics import CreateAPIView
from rest_framework.views import APIView
from django.shortcuts import redirect

from chat.forms import CustomUserCreationForm
from chat.models import Message,Chat,Participant
from chat.serializers import MessageSerializer,ChatSerializer,ParticipantSerializer,UserSerializer
from rest_framework.response import Response
from django.contrib.auth.models import User
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt


class UserRegisterView(generic.CreateView):
    form_class = CustomUserCreationForm
    template_name = 'registration/register.html'
    success_url = reverse_lazy('login')


class ChatView(View):
    def get(self, request, *args, **kwargs):
        return render(request,'index.html')


def redirect_view(request):
    response = redirect('index')
    return response

def handler404(request, exception):
    return render(request, "error_handler/404error.html")


class MessageAPIView(generics.ListAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer


class ChatAPIView(APIView):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self,request,formant= None):
        queryset = Chat.objects.all()
        serializer = ChatSerializer(queryset,many=True)
        return Response(serializer.data)
    
    def post(self,request,format=None):
        serializer =ChatSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save() 
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class ParticipantAPIView(APIView):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self,request,formant= None):
        queryset = Participant.objects.all()
        serializer = ParticipantSerializer(queryset,many=True)
        return Response(serializer.data)

    def post(self,request,format=None):
        serializer =ParticipantSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save() 
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CurrentUserView(APIView):
    def get(self, request):
        self.serializer = UserSerializer(request.user)  
        return Response(self.serializer.data)


        


class GetUsersView(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
