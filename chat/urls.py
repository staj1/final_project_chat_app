from django.contrib.auth.decorators import login_required
from django.urls import path

from chat.views import ChatView, CurrentUserView
from chat.views import MessageAPIView , ChatAPIView , ParticipantAPIView, GetUsersView
urlpatterns = [
    path('', login_required(ChatView.as_view(), login_url='/login/'), name='index'),
    path('api/message/',MessageAPIView.as_view(),name='api_message'),
    path('api/chat/',ChatAPIView.as_view(),name='api_chat'),
    path('api/participant/',ParticipantAPIView.as_view(),name='api_participant'),
    path('api/current/',CurrentUserView.as_view(),name='api_user'),
    path('api/all_users/',GetUsersView.as_view(),name='api_all_users'),
    path('<uuid:token>/',login_required(ChatView.as_view(), login_url='/login/'), name='private_chat'),
]
