from django.contrib import admin

from chat.models import Message,Chat,Participant


class MessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'text', 'sender','chat', 'created_at')

class ChatAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'creator', 'created_at','guid')

class ParticipantAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'chat', 'created_at','read_only')


admin.site.register(Message,MessageAdmin)
admin.site.register(Chat,ChatAdmin)
admin.site.register(Participant,ParticipantAdmin)

