from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import User
from django.db import models
from uuid import uuid4


class Chat(models.Model):
    title = models.CharField(unique=True, max_length=25, help_text='Enter Group Name', null=False, blank=False,default='My Group')
    creator = models.ForeignKey(User, related_name='group_creator', on_delete=models.SET_NULL, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    guid = models.UUIDField(unique=True, default=uuid4, editable=False, verbose_name="Unique ID",help_text="This field is automatically determined by the system.")

    def __str__(self):
        return ' Chat with {}'.format(self.title)

class Participant(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=False)
    chat = models.ForeignKey(Chat, on_delete=models.SET_NULL, null=True, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)
    # if user leaves the chat then read_only=True, that means he can still reach the chat but cant send anything ,
    # cant see new messages
    read_only = models.BooleanField(default=False, null=False, blank=False)

    def __str__(self):
        return 'User: {} Chat: {}'.format(self.user, self.chat)

class Message(models.Model):
    img = 'image'
    text ='text'
    TYPE_CHOICES =( 
    (text, "text"), 
    (img, "image"), 
 
) 
    type = models.CharField(max_length=10,choices=TYPE_CHOICES,default=text)
    text = models.CharField(max_length=500, blank=False, null=False)
    sender = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=False)
    chat = models.ForeignKey(Chat, on_delete=models.SET_NULL, null=True, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return self.text

#class CustomUser ():
