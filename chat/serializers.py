from rest_framework import fields, serializers
from chat.models import Message,Chat,Participant
from django.contrib.auth.models import User
from allauth.socialaccount.models import SocialAccount

class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ('id','type', 'text', 'sender', 'chat','created_at',)

class ChatSerializer(serializers.ModelSerializer):
    class Meta:
        model = Chat
        fields = ('id','creator' ,'title', 'created_at', 'guid')

class ParticipantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Participant
        fields = ('id', 'user', 'chat', 'created_at', 'read_only')

class UserSerializer(serializers.ModelSerializer):
    picture = serializers.SerializerMethodField('get_extra_data')
    class Meta:
        model = User
        fields=('id','username','first_name','last_name','email','picture','is_staff','is_active','date_joined','last_login',)

    def get_extra_data(self,request):
        if SocialAccount.objects.filter(user_id=request.id).exists():
            current_social_account=SocialAccount.objects.filter(user_id=request.id)
            picture=current_social_account[0].extra_data['picture']
            return picture
        else:
            return 'No social account'


