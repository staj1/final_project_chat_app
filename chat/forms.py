from django import forms
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm, PasswordResetForm
from django.contrib.auth.models import User


# REGISTER FORM #
class CustomUserCreationForm(UserCreationForm):
    terms_of_service = forms.BooleanField(widget=forms.CheckboxInput(
        attrs={'class': 'custom-control-input', 'id': 'customCheck1'}),
        required=True,
        error_messages={
            'required': "You must agree to the terms to register"})

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password1', 'password2', ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update(
            {'class': 'form-control pl-5', 'placeholder': 'Enter Username', 'name': 'username', 'required': ''})
        self.fields['first_name'].widget.attrs.update(
            {'class': 'form-control pl-5', 'placeholder': 'Enter First Name', 'name': 'first_name', 'required': ''})
        self.fields['last_name'].widget.attrs.update(
            {'class': 'form-control pl-5', 'placeholder': 'Enter Last Name', 'name': 'last_name', 'required': ''})
        self.fields['email'].widget.attrs.update(
            {'class': 'form-control pl-5', 'placeholder': 'Enter Email', 'required': ''})
        self.fields['email'].required = True
        self.fields['password1'].widget.attrs.update(
            {'class': 'form-control pl-5', 'placeholder': 'Enter Password', 'required': ''})
        self.fields['password2'].widget.attrs.update(
            {'class': 'form-control pl-5', 'placeholder': 'Enter Password', 'required': ''})


# LOGIN FORM #
class CustomLoginForm(AuthenticationForm):
    pass
    



class CustomPasswordResetForm(PasswordResetForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['email'].widget.attrs.update(
            {'class': 'form-control pl-5', 'placeholder': 'Enter Email', 'name': 'email', 'required': ''})

    # TODO celery implementation
    # https://stackoverflow.com/questions/54202009/how-to-send-djangos-password-reset-email-using-celery-without-third-party-pack
    # https://github.com/pmclanahan/django-celery-email
    # def send_mail(self, subject_template_name, email_template_name,
    #               context, from_email, to_email, html_email_template_name=None):
