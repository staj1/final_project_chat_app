"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.urls import path, include
from django.contrib.auth import views as auth_views
from chat.views import UserRegisterView ,redirect_view
from chat.forms import CustomLoginForm, CustomPasswordResetForm


urlpatterns = [
    path('',redirect_view,name='login-redirect'),
    path('admin/', admin.site.urls),
    path('chat/', include('chat.urls')),
    #Login, Register
    path('register/', UserRegisterView.as_view(), name='register'),
    path("login/", auth_views.LoginView.as_view(authentication_form=CustomLoginForm, redirect_authenticated_user=True),
         name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    # password reset
    path("recover/", auth_views.PasswordResetView.as_view(form_class=CustomPasswordResetForm), name='password_reset'),
    path('recover/done/', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('recover/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('recover/complete/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
    #Allauth
    path('accounts/', include('allauth.urls')),

]
handler404 = 'chat.views.handler404'
