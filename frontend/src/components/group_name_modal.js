
import React, { useState, useEffect } from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Label, Form, FormGroup, Input } from 'reactstrap';
import axios from 'axios';
import Cookies from 'js-cookie';
import CreateParticipantModal from './participants_modal';

function CreateGroupModal({ openState, toggle }) {

    const [PmodalState, setPModalState] = useState(false);
    const togglePModal = () => setPModalState(!PmodalState);
    const [userId, setUserId] = useState('');
    const [userName, setUserName] = useState('');



    useEffect(() => {
        const get_current_user = async () => {
          const result = await axios('http://127.0.0.1:8000/chat/api/current');
          console.log(result.data)
          setUserId(result.data.id);
          setUserName(result.data.first_name+" "+result.data.last_name);
        };
        get_current_user();
    
      }, []);
    const handleSubmit = (e) => {
        e.preventDefault();
        var csrfCookie = Cookies.get('csrftoken');
        axios({
            method: 'post',
            url: 'http://127.0.0.1:8000/chat/api/chat/',
            /* TODO : creator and Date is going to be fixed */
            data: { title: document.getElementById('group_name').value, creator: userId, created_at: new Date() },
            headers: { 'X-CSRFTOKEN': csrfCookie, },
        });
    };
    const handleClick = (e) => {
        handleSubmit(e);
        toggle();
        
    };

    return (
        <div>
            <Modal isOpen={openState} >
                <ModalHeader >Create New Group</ModalHeader>
                <ModalBody>
                    <Form onSubmit={handleSubmit}>
                        <FormGroup>
                            <Label for="group_name">Group Name</Label>
                            <Input type="text" id="group_name" placeholder="Enter a Group Name" />
                        </FormGroup>
                        <ModalFooter>
                            <Button color="primary" type='Submit' onClick={handleClick}>Create Group</Button>
                            <Button color="danger" onClick={toggle}>Cancel</Button>
                        </ModalFooter>
                    </Form>
                </ModalBody>
            </Modal>
            {/*<CreateParticipantModal openState={PmodalState} toggle={togglePModal} handle/>*/}
        </div>
    );
}

export default CreateGroupModal
