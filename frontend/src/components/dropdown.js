import React, { useState } from 'react';
import CreateGroupModal from './group_name_modal';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';



function MoreVertDropdown({ openState, toggle }) {
    const [modalState, setModalState] = useState(false);
    const toggleModal = () => setModalState(!modalState);


    return (
        <>
            <Dropdown isOpen={openState} toggle={toggle}>
                <DropdownToggle
                    tag="span"
                    data-toggle="dropdown"
                    aria-expanded={openState}>
                </DropdownToggle>
                <DropdownMenu>
                    <DropdownItem header >Menu</DropdownItem>
                    <DropdownItem disabled>Edit Profile</DropdownItem>
                    <DropdownItem onClick={toggleModal}> Create New Chat</DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem >Another Action</DropdownItem>
                </DropdownMenu>
            </Dropdown>
            <CreateGroupModal  openState={modalState} toggle={toggleModal} />
        </>

    );
}

export default MoreVertDropdown;