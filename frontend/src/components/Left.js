import React, { useEffect, useState } from 'react';
import './Left.css';
import { Avatar, IconButton } from '@material-ui/core';
import DonutLargeIcon from '@material-ui/icons/DonutLarge';
import ChatIcon from '@material-ui/icons/Chat';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import SearchOutlinedIcon from '@material-ui/icons/SearchOutlined';
import LeftChat from './LeftChat';
import axios from 'axios';
import MoreVertDropdown from './dropdown';



function Left({ current_user_id, current_user_name, current_user_pic }) {
    const [chats, setChats] = useState([]);
    const [subscriptions, setSubs] = useState([]);
    const [dropdownState, setDropdownState] = useState(false);
    const toggleDropdown = () => setDropdownState(dropdownState => !dropdownState);

    useEffect(() => {
        const fetch = async () => {
            const result = await axios('http://127.0.0.1:8000/chat/api/chat',);
            setChats(await result.data.map((chat) => ({ id: chat.id, creator: chat.creator, title: chat.title, guid: chat.guid, created_at: chat.created_at, })));
        };
        fetch();
    });

    useEffect(() => {
        const fetchP = async () => {
            const result = await axios('http://127.0.0.1:8000/chat/api/participant',);
            setSubs(await result.data.map((participant) => participant.chat));
        };
        fetchP();
    }, []);
    return (
        <>
            <div className='left'>
                <div className='left_header'>
                    <div className='left_header_user'>
                        <Avatar src={current_user_pic} />
                        <p>{current_user_name}</p>
                    </div>
                    <div className='left_headerRight'>
                        <IconButton>
                            <DonutLargeIcon />
                        </IconButton>
                        <IconButton>
                            <ChatIcon />
                        </IconButton>
                        <IconButton onClick={() => { setDropdownState(!dropdownState) }}>
                            <MoreVertIcon />
                        </IconButton>

                    </div>
                </div>
                <div className='left_search'>
                    <div className='left_searchContainer'>
                        <SearchOutlinedIcon />
                        <input placeholder='Search or start a new chat' type='text' />
                    </div>
                </div>
                <div className='left_chats'>
                    {chats.map(chat => (<LeftChat key={chat.id} title={chat.title} id={chat.id} creator={chat.creator} guid={chat.guid} />))}
                </div>
            </div>
            <MoreVertDropdown  openState={dropdownState} toggle={toggleDropdown} />
        </>
    )
}

export default Left
