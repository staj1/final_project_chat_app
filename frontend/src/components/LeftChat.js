import React, { useEffect, useState } from 'react';
import './LeftChat.css';
import { Avatar } from '@material-ui/core';
import {Link} from 'react-router-dom';

function LeftChat({title,id,creator,guid}) {

    const [seed, setSeed] = useState('')
    useEffect(() => {
        setSeed(Math.floor(Math.random() * 5000))
    }, [])

    return (
        <Link to={`/chat/${guid}`}>
            <div className='leftChat'>
                <Avatar src={`https://avatars.dicebear.com/api/human/${seed}.svg `} />
                <div className='leftChat_info'>
                    <h2>{title}</h2>
                    <p>Last Message</p>
                </div>
            </div>
        </Link>
    )
}

export default LeftChat
