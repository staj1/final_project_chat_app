import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import ReactDOM from 'react-dom';
import '../../static/css/App.css';
import Left from './Left';
import Chat from './Chat';
import axios from 'axios';



function App() {
  const [userId, setUserId] = useState('');
  const [userName, setUserName] = useState('');
  const [picUrl,setPicUrl]= useState('');
  const [participants, setParticipants] = useState([]);

  useEffect(() => {
    const get_current_user = async () => {
      const result = await axios('http://127.0.0.1:8000/chat/api/current');
      console.log(result.data)
      setUserId(result.data.id);
      setUserName(result.data.first_name+" "+result.data.last_name);
      setPicUrl(result.data.picture);
    };
    get_current_user();

  }, []);

  useEffect(() => {
    const fetchParticipant = async () => {
      const result = await axios(
        'http://127.0.0.1:8000/chat/api/participant',
      );

      setParticipants(result.data.map((participant) => ({ user_id: participant.user, chat_id: participant.chat, })));
    };
    fetchParticipant();

  }, []);
  return (
    <div className='_app'>
      <div className='app_body'>
        <Router>
          <Left current_user_id={userId} current_user_name={userName} current_user_pic={picUrl}/>
          <Route path='/chat/:guid'>
            <Chat current_user_name={userName} current_user_id={userId} />
          </Route>
        </Router>
      </div>
    </div>
  );
}
export default App

ReactDOM.render(<App />, document.getElementById('app'))