import React, { useEffect, useState, useRef } from 'react';
import { useParams } from 'react-router-dom';
import './Chat.css';
import { Avatar, IconButton } from '@material-ui/core';
import { AttachFile, InsertEmoticon, MoreVert, SearchOutlined, Mic, Image } from '@material-ui/icons';
import axios from 'axios';
import { w3cwebsocket as W3CWebSocket } from "websocket";
import "emoji-mart/css/emoji-mart.css";
import { Picker } from "emoji-mart";

function Chat({ current_user_id, current_user_name }) {
    const [input, setInput] = useState('');
    const [seed, setSeed] = useState('');
    const { guid } = useParams();
    const [chatID, setChatID] = useState('');
    const [chatTitle, setChatTitle] = useState('');
    const [messages, setMessages] = useState([]);
    const ws = useRef(null);
    const [file, setFile] = useState(null)
    const [emojiPickerState, SetEmojiPicker] = useState(false);
    //`ws://` + window.location.host + `/chat/${guid}/`
    useEffect(() => {
        ws.current = new WebSocket('ws://echo.websocket.org');
        ws.current.onopen = () => console.log("ws opened");
        ws.current.onclose = (e) => console.log("ws closed", e.code);
        ws.current.onerror = (e) => console.log(`Error: ${e.message}`);
        return () => {
            ws.current.close();
        };
    }, []);

    useEffect(() => {
        if (!ws.current) return;
        ws.current.onmessage = e => {
            const message = JSON.parse(e.data);
            if (message['type'] === 'text') {
                console.log('textMessage have come')
                setMessages(messages => [...messages, { text: message.text,type:message.type, sender: message.sender, created_at: message.created_at }]);
            } else {
                console.log('TYPE:' + message['type'] + 'have come');
                setMessages(messages => [...messages, { text: '', type:message.type, base64: message.base64, sender: message.sender, created_at: message.created_at }]);


            }


        };
    }, []);

    useEffect(() => {
        setSeed(Math.floor(Math.random() * 5000))
    }, [guid])

    useEffect(() => {
        if (guid) {
            const fetchChatData = async () => {
                const result = await axios('http://127.0.0.1:8000/chat/api/chat');
                const filterChat = result.data.filter(chat => chat.guid == guid);
                setChatID(filterChat.map(chat => chat.id));
                setChatTitle(filterChat.map(chat => chat.title));
            };
            fetchChatData();
        }
    },[]);


    useEffect(() => {
        const fetchMessages = async () => {
            const result = await axios('http://127.0.0.1:8000/chat/api/message',);
            const filterMessage = result.data.filter(message => message.chat == chatID);
            setMessages(filterMessage.map((message) => ({ text: message.text,type:message.type, sender: message.sender, created_at: message.created_at, })));
        };
        fetchMessages();
    }, [chatID]);

    const sendMessage = (e) => {
        e.preventDefault();
        const current_time = new Date();
        current_time.setHours(current_time.getHours() + 3);
        ws.current.send(JSON.stringify({ type: "text", text: input, sender: current_user_id, created_at: current_time }));
        setInput('');
    };
    const handleImage = (e) => {
        setFile(e.target.files[0]);
        var data = e.target.files[0];
        const reader = new FileReader();
        const current_time = new Date();
        current_time.setHours(current_time.getHours() + 3);
        reader.onload = (e) => {
            ws.current.send(JSON.stringify({ type: "image", base64: reader.result, sender: current_user_id, created_at: current_time }));
        }
        console.log('handleImage')
        reader.readAsDataURL(data);

    };
    let emojiPicker;
    if (emojiPickerState) {
        emojiPicker = (
            <Picker style={{ position: "absolute", bottom: 70, left: 5, }}
                showPreview={false} showSkinTones={false} onSelect={emoji => setInput(input + emoji.native)} />
        );
    }
    const handleEmoji = () => {
        SetEmojiPicker(!emojiPickerState);
    };
    return (
        <div className='chat'>
            <div className='chat_header'>
                <Avatar src={`https://avatars.dicebear.com/api/human/${seed}.svg `} />
                <div className='chat_headerInfo'>
                    <h3>{chatTitle}</h3>
                    <p>Last Seen</p>
                </div>
                <div className='chat_headerRight'>
                    <IconButton>
                        <SearchOutlined />
                    </IconButton>
                    <IconButton>
                        <AttachFile />
                    </IconButton>
                    <IconButton>
                        <MoreVert />
                    </IconButton>
                </div>
            </div>
            <div className='chat_body'>
                {messages.map((message, index) => (message.type === "text") ?
                    (

                        <p key={index} className={`chat_message ${current_user_id == message.sender && 'chat_receiver'}`}>
                            <span className='chat_name'>{message.sender}</span>
                            {message.text}
                            <span className='chat_timestamp'>{message.created_at}</span>
                        </p>

                    ) : (
                        <p key={index} className={`chat_message ${current_user_id == message.sender && 'chat_receiver'}`}>
                            <span className='chat_name'>{message.sender}</span>
                            <span><img src={`${message.base64}`} /></span>
                            <span className='chat_timestamp'>{message.created_at}</span>
                        </p>
                    ))}

            </div>
            <div className='chat_footer'>
                <IconButton aria-label="insert emoji" component="span" onClick={handleEmoji}>
                    <InsertEmoticon />
                </IconButton>
                {emojiPicker}
                <input accept="image/*" id="icon-button-file" type="file" files={file} onChange={handleImage} />
                <label htmlFor="icon-button-file">
                    <IconButton aria-label="upload picture" component="span">
                        <Image />
                    </IconButton>
                </label>
                <form>
                    <input value={input} onChange={e => setInput(e.target.value)} type='text' placeholder='Type a message' />
                    <button onClick={sendMessage} type='submit'>Send a Message</button>
                </form>
                <Mic />
            </div>
        </div>
    )
}

export default Chat
