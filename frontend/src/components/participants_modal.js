
import React, { useState, useEffect } from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Label, Form, FormGroup, Input } from 'reactstrap';
import Select from "react-select";
import axios from 'axios';
import Cookies from 'js-cookie';
import makeAnimated from "react-select/animated";

const animatedComponents = makeAnimated();

function CreateParticipantModal({ openState, toggle }) {
    const [selectedUsers, setSelectedUsers] = useState([]);
    const ID = [];
    const [users, setUsers] = useState([]);

    const handleSubmit = (e) => {
        e.preventDefault();
        var csrfCookie = Cookies.get('csrftoken');
        ID.push(selectedUsers.map(obj => obj.value));
        console.log(ID);
/*         for (var i = 0; i < ID.length; i++) {
            axios({
                method: 'post',
                url: 'http://127.0.0.1:8000/chat/api/participant/',
                data: { chat: 2, user: ID[i] },
                headers: { 'X-CSRFTOKEN': csrfCookie, },
            });
        } */



    };

    const handleClick = (e) => {
        handleSubmit(e);
        toggle();
    };


    useEffect(() => {
        const fetchChatData = async () => {
            const result = await axios('http://127.0.0.1:8000/chat/api/all_users');
            setUsers(result.data.map(user => ({
                value: user.id,
                label: user.username,
            })));

        };
        fetchChatData();

    }, []);


    return (
        <div>
            <Modal isOpen={openState} >
                <ModalHeader >Select Users</ModalHeader>
                <ModalBody>
                    <Form onSubmit={handleSubmit}>
                        <FormGroup>
                            <Label for="dropdown_users">Select Users</Label>
                            <Select onChange={setSelectedUsers} defaultValue={selectedUsers} closeMenuOnSelect={false} components={animatedComponents} isMulti options={users} />
                        </FormGroup>
                        <ModalFooter>
                            <Button color="primary" type='Submit' onClick={handleClick}>Do Something</Button>
                            <Button color="danger" onClick={toggle}>Cancel</Button>
                        </ModalFooter>
                    </Form>
                </ModalBody>
            </Modal>

        </div>
    );
}

export default CreateParticipantModal
